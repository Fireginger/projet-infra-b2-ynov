# installation d'owncloud

## pré-requis lamp

### apache
```
sudo dnf install httpd
sudo systemctl enable httpd
sudo systemctl start httpd
sudo systemctl status httpd
```

### mariadb
```
sudo dnf install mariadb-server mariadb
sudo systemctl enable --now mariadb
sudo systemctl start mariadb
sudo systemctl status mariadb
```

### php
```
sudo dnf module list php
sudo dnf module install php:7.4
sudo dnf install php-curl php-gd php-intl php-json php-ldap php-mbstring php-mysqlnd php-xml php-zip php-opcache
```

## Firewall 
```
sudo firewall-cmd --add-service=http --permanent
sudo firewall-cmd --reload
```
## configuration de mysql

- ``` sudo mysql -u root -p ```
```
CREATE DATABASE owncloud_db;
CREATE USER 'owncloud_user'@'localhost' IDENTIFIED BY 'P@ssword';
GRANT ALL ON owncloud_db.* TO 'owncloud_user'@'localhost';
FLUSH PRIVILEGES;
exit;
```

## Installation de owncloud 

```
wget https://download.owncloud.com/server/owncloud-complete-20220919.tar.bz2
```

```
sudo tar -jxf owncloud-complete-20220919.tar.bz2 -C
/var/www/html
```

```
sudo chown apache:apache -R /var/www/html/owncloud
```

```
sudo chmod -R 775 /var/www/html/owncloud
```

## configuration d'apache

```
sudo vim /etc/httpd/conf.d/ownc
loud.conf
```
```
Alias /owncloud "/var/www/html/owncloud/"

<Directory /var/www/html/owncloud/>
  Options +FollowSymlinks
  AllowOverride All

 <IfModule mod_dav.c>
  Dav off
 </IfModule>

 SetEnv HOME /var/www/html/owncloud
 SetEnv HTTP_HOME /var/www/html/owncloud

</Directory>
```

```
sudo systemctl restart httpd
sudo systemctl status httpd
```
