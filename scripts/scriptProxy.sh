sudo dnf update -y
sudo dnf install gcc-c++ flex bison yajl curl-devel curl zlib-devel pcre-devel autoconf automake git curl make libxml2-devel pkgconfig libtool httpd-devel redhat-rpm-config git wget openssl openssl-devel vim -y
sudo dnf --enablerepo=powertools install doxygen yajl-devel -y
sudo dnf install epel-release https://rpms.remirepo.net/enterprise/remi-release-8.rpm -y
sudo dnf --enablerepo=remi install GeoIP-devel -y
sudo mkdir /tmp/modsec
cd /tmp/modsec
sudo git clone --depth 1 -b v3/master --single-branch https://github.com/SpiderLabs/ModSecurity
cd ModSecurity
sudo git submodule init
sudo git submodule update
sudo ./build.sh
sudo ./configure
sudo make
sudo make install
cd
cd /tmp/modsec
sudo git clone https://github.com/SpiderLabs/ModSecurity-nginx.git
sudo wget http://nginx.org/download/nginx-1.19.10.tar.gz
sudo tar xzf nginx-1.19.10.tar.gz
sudo useradd -r -M -s /sbin/nologin -d /usr/local/nginx nginx
cd nginx-1.19.10
sudo ./configure --user=nginx --group=nginx --with-pcre-jit --with-debug --with-http_ssl_module --with-http_realip_module --add-module=/tmp/modsec/ModSecurity-nginx
sudo make
sudo make install
sudo cp /tmp/modsec/ModSecurity/modsecurity.conf-recommended /usr/local/nginx/conf/modsecurity.conf
sudo cp /tmp/modsec/ModSecurity/unicode.mapping /usr/local/nginx/conf/
sudo cp /usr/local/nginx/conf/nginx.conf{,.bak}

mkdir /var/log/nginx

sudo cat > /etc/systemd/system/nginx.service << 'EOL'
[Unit]
Description=The nginx HTTP and reverse proxy server
After=network.target remote-fs.target nss-lookup.target

[Service]
Type=forking
ExecStartPre=/usr/bin/rm -f /run/nginx.pid
ExecStartPre=/usr/sbin/nginx -t
ExecStart=/usr/sbin/nginx
ExecReload=/bin/kill -s HUP $MAINPID
KillSignal=SIGQUIT
TimeoutStopSec=5
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target
EOL

sudo ln -s /usr/local/nginx/sbin/nginx /usr/sbin/


sudo sed -i 's/SecRuleEngine DetectionOnly/SecRuleEngine On/' /usr/local/nginx/conf/modsecurity.conf
sudo sed -i 's#/var/log/modsec_audit.log#/var/log/nginx/modsec_audit.log#' /usr/local/nginx/conf/modsecurity.conf
sudo git clone https://github.com/SpiderLabs/owasp-modsecurity-crs.git /usr/local/nginx/conf/owasp-crs
sudo cp /usr/local/nginx/conf/owasp-crs/crs-setup.conf{.example,}

sudo firewall-cmd --add-port=80/tcp --permanent
sudo firewall-cmd --reload