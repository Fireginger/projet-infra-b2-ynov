# Projet Infra B2 Ynov

## Groupe

- [x] [Thomas EVEILLARD]
- [x] [Baptiste GADEBILLE]

## Introduction

Le projet consiste en la mise en place d'une infrastructure pour un bastion Guacamole et Tomcat. Cette infrastructure est conçue pour fournir un accès à distance sécurisé et centralisé à des ordinateurs distants.
Nous avons ensuite rajouté différents services pour rendre l'infrastructure plus complète et plus sécurisée.


## Architecture

Machine     |      IP     |   Rôle
------------|-------------|----------
Guacamole   | 10.102.1.40 | Guacamole et Tomcat
DB          | 10.102.1.30 | Base de données
Proxy       | 10.102.1.10 | Reverse Proxy
Test        | 10.102.1.20 | Machine de test pour acceder à Guacamole et Tomcat



## Installation


### DB

Pour installer la base de données, nous avons créee un script qui permet d'installer mariadb. Ce script est disponible sur le dépot du projet :

Quand nous avons installé mariaDB, il nous a fallu créer une base de données et un utilisateur pour pouvoir utiliser Guacamole. Pour cela, nous avons utilisé la commande suivante :

```SQL
mysql -u root -p 
(aucun mot de passe requis)

create database guacdb23;

create user 'gcadmin_23'@'%' identified by 'password';

grant ALL PRIVILEGES on guacdb23.* to 'gcadmin_23'@'%';

flush privileges;

quit
```

Puis on redémmare le service mariadb pour que les modifications soient prises en compte.

```bash
systemctl restart mariadb
```


### Guacamole et Tomcat

Pour l'installation, nous avons créee un script qui permet d'installer et de configurer les différents services automatiquement. Ce script est disponible sur le dépot du projet.


Les seuls choses à faire sont :

- Penser à écrire dans le terminale guacd à la fin du script pour bien vérifier que guacamole est bien installé et lancé.

- Restart de nouveau MariaDB sur la machine db pour que les modifications soient prises en compte et bien vérifier que les services Tomcat, Guacd et MariaDB tournent sans problèmes.


Ensuite on peut se connecter à Guacamole via l'adresse suivante : http://10.102.1.40:8080/guacamole.


### Reverse Proxy 

Pour mettre en place le reverser proxy on va utiliser une autre machine (voir tableau des machines), sur laquelle on va installer Nginx grâce à un script disponible dans le dépot du projet.

Puis on va venir mettre dans la configuration de nginx au niveau du "location / {" dans le bloc server :

```
location /guacamole/ {
    proxy_pass http://10.102.1.40:8080;
    proxy_buffering off;
    proxy_http_version 1.1;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection $http_connection;
    access_log off;
}
```

Et dans le bloc server les lignes :

```
modsecurity  on;
modsecurity_rules_file  /usr/local/nginx/conf/modsecurity.conf;
```

Qui vont permettre d'activer modsecurity sur notre serveur.

Ensuite, il nous reste plus qu'a démarrer et faire que notre service se lance au démarrage de la machine :
```
sudo systemctl enable --now nginx
```

Et de rajouter dans la configuration de modsecurity ( qui se trouve en /usr/local/nginx/conf/modsecurity.conf) les lignes suivantes :
```
Include owasp-crs/crs-setup.conf
Include owasp-crs/rules/*.conf
```

Ensuite on pense a restart le service Nginx pour que les changements sois pris en compte puis on peut se connecter à notre guacamole avec l'url : http://10.102.1.10/guacamole




## Sécurisation

Maintenant que toutes nos machines sont installés, il nous reste plus qu'a sécuriser tout ca grâce à différents moyens.

### Monitoring

Pour le monitoring on va utiliser Netdata sur nos machines (proxy, guacamole et database) et paramétrer les alertes pour savoir en permanence si nos machines ne sont pas surchargés ou ont un problème.

Pour cela on va utiliser le script de Monitoring qu'il y a dans le dépot du projet.

Ensuite on va aller sur l'adresse de netdata sur notre machine http://AdresseIP:19999 .


Pour aller plus loin on peut configurer les alertes de netdata sur discord pour être notifié en permanence si un problème survient sur nos machines. Pour faire ca, il nous suffit juste de créer un webhook sur discord puis d'aller dans la configuration de netdata et faire :

```
[thomas@localhost ~]$ sudo cat /etc/netdata/health_alarm_notify.conf
# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discordapp.com/api/webhooks/.../..."

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):

DEFAULT_RECIPIENT_DISCORD="général"
```
En remplacant bien sur le lien du webhook par le lien que vous avez créé sur discord.

Et voila, maintenant on est notifié sur discord si un problème survient sur nos machines :

![Discord](/images/capture%20d'%C3%A9cran%20netdata%202023.png)



## Images

Voici une image de l'écran d'accueil de guacamole :

![Guacamole](/images/écran%20accueil%20guacamole.png)


Voici une image du panel admin de guacamole :

![Guacamole](/images/panel%20admin%20guacamole.png)

Un [axe d'amelioration](https://gitlab.com/Fireginger/projet-infra-b2-ynov/-/blob/main/Axe%20d'am%C3%A9lioration/Axe_d_am%C3%A9liorations.md) possible a notre projet pourrait être d'ajouter un service comme owncloud via des conteneurs docker
