#!/bin/bash

# Install required dependencies
sudo dnf install epel-release -y
sudo dnf config-manager --set-enabled powertools
sudo dnf install -y unzip curl make cmake wget gcc zlib-devel compat-openssl10 cairo-devel libjpeg-turbo-devel \
libpng-devel libtool uuid-devel freerdp-devel pango-devel libssh2-devel libtelnet-devel libvncserver-devel \
libwebsockets-devel pulseaudio-libs-devel openssl-devel libvorbis-devel libwebp-devel wget vim

# Install Java 11
sudo dnf install java-11-openjdk-devel -y

# Create tomcat user and directory
sudo useradd -d /usr/share/tomcat -M -r -s /bin/false tomcat
sudo mkdir /usr/share/tomcat

# Download and install Apache Tomcat 9
sudo wget https://downloads.apache.org/tomcat/tomcat-9/v9.0.72/bin/apache-tomcat-9.0.72.tar.gz
sudo tar xzf apache-tomcat-9.0.72.tar.gz -C /usr/share/tomcat --strip-components=1
sudo chown -R tomcat:tomcat /usr/share/tomcat

# Create and enable systemd service for Tomcat
sudo cat >/etc/systemd/system/tomcat.service <<EOF
[Unit]
Description=Tomcat Server
After=syslog.target network.target

[Service]
Type=forking
User=tomcat
Group=tomcat

Environment=JAVA_HOME=/usr/lib/jvm/jre
Environment='JAVA_OPTS=-Djava.awt.headless=true'
Environment=CATALINA_HOME=/usr/share/tomcat
Environment=CATALINA_BASE=/usr/share/tomcat
Environment=CATALINA_PID=/usr/share/tomcat/temp/tomcat.pid
Environment='CATALINA_OPTS=-Xms512M -Xmx1024M'
ExecStart=/usr/share/tomcat/bin/catalina.sh start
ExecStop=/usr/share/tomcat/bin/catalina.sh stop

[Install]
WantedBy=multi-user.target
EOF

sudo systemctl daemon-reload
sudo systemctl start tomcat
sudo systemctl enable tomcat

# Download, build and enable guacamole server
sudo wget https://downloads.apache.org/guacamole/1.5.0/source/guacamole-server-1.5.0.tar.gz
sudo tar xzf guacamole-server-1.5.0.tar.gz
cd guacamole-server-1.5.0
./configure --with-init-dir=/etc/init.d
make
make install
ldconfig
sudo systemctl start guacd
sudo systemctl enable guacd

# Create Guacamole config directory and download Guacamole war file
sudo mkdir /etc/guacamole
sudo wget https://downloads.apache.org/guacamole/1.5.0/binary/guacamole-1.5.0.war -O /etc/guacamole/guacamole.war

# Create symlink for Guacamole war file and restart Tomcat
sudo ln -s /etc/guacamole/guacamole.war /usr/share/tomcat/webapps/
sudo systemctl restart tomcat

# Set GUACAMOLE_HOME environment variable
cat >/etc/default/tomcat <<EOF
GUACAMOLE_HOME=/etc/guacamole
EOF

# Créer le fichier guacamole.properties
sudo tee /etc/guacamole/guacamole.properties << EOF
guacd-hostname: localhost
guacd-port:     4822
user-mapping:   /etc/guacamole/user-mapping.xml
auth-provider:  net.sourceforge.guacamole.net.basic.BasicFileAuthenticationProvider
EOF

# Créer le fichier user-mapping.xml
sudo tee /etc/guacamole/user-mapping.xml << EOF
<user-mapping>

    <!-- Per-user authentication and config information -->

    <!-- A user using md5 to hash the password
         guacadmin user and its md5 hashed password below is used to 
             login to Guacamole Web UI-->
    <authorize 
            username="guacadmin"
            password="5f4dcc3b5aa765d61d8327deb882cf99"
            encoding="md5">

        <!-- First authorized Remote connection -->
        <connection name="Rocky linux 8 Server">
            <protocol>ssh</protocol>
            <param name="hostname">10.102.1.20</param>
            <param name="port">22</param>
        </connection>

        <!-- Second authorized remote connection -->
        <connection name="Windows 11">
            <protocol>rdp</protocol>
            <param name="hostname">192.168.1.31</param>
            <param name="port">3389</param>
            <param name="username">thomas</param>
            <param name="ignore-cert">true</param>
        </connection>

    </authorize>

</user-mapping>
EOF

# Redémarrer les services
sudo systemctl restart tomcat guacd

# Ouvrir le port 8080 dans le firewall
sudo firewall-cmd --add-port=8080/tcp --permanent
sudo firewall-cmd --add-port=4822/tcp --permanent
sudo firewall-cmd --add-port=3306/tcp --permanent
sudo firewall-cmd --reload

sudo dnf install mariadb -y

sudo wget https://dlcdn.apache.org/guacamole/1.5.0/binary/guacamole-auth-jdbc-1.5.0.tar.gz

sudo mkdir /etc/guacamole/extensions

sudo mkdir /etc/guacamole/lib

sudo tar xzf guacamole-auth-jdbc-1.5.0.tar.gz guacamole-auth-jdbc-1.5.0/mysql

sudo cp guacamole-auth-jdbc-1.5.0/mysql/guacamole-auth-jdbc-mysql-1.5.0.jar /etc/guacamole/extensions/

mysql -h 10.102.1.30 -u gcadmin_23 --password=password guacdb23 < guacamole-auth-jdbc-1.5.0/mysql/schema/001-create-schema.sql

mysql -h 10.102.1.30 -u gcadmin_23 --password=password guacdb23 < guacamole-auth-jdbc-1.5.0/mysql/schema/002-create-admin-user.sql

sudo wget https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-j-8.0.31.tar.gz

sudo tar xzvf mysql-connector-j-8.0.31.tar.gz

sudo cp mysql-connector-j-8.0.31/mysql-connector-j-8.0.31.jar /etc/guacamole/lib


cat >> /etc/guacamole/guacamole.properties <<EOL
mysql-hostname: 10.102.1.30
mysql-database: guacdb23
mysql-username: gcadmin_23
mysql-password: password
EOL

systemctl restart tomcat guacd

sudo wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp netdata-kickstart.sh --disable-telemetry

sudo firewall-cmd --add-port=19999/tcp --permanent
sudo firewall-cmd --reload


sudo systemctl start netdata
sudo systemctl enable netdata