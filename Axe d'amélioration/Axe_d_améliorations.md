# Axe d'améliorations :

Le but de cette axe d'amelioration est de déployer Owncloud (une application libre et open source de stockage et de partage de fichiers) sans utiliser l'images officiel owncloud de docker sinon ce serait un peut de la triche.

## Firewall 

sudo firewall-cmd --zone=public --add-port=80/tcp --permanent
sudo firewall-cmd --reload

## Installation de docker:

```
sudo dnf install docker-ce wget -y
sudo systemctl enable docker
sudo systemctl start docker
sudo systemctl status docker
```

```
sudo curl -L "https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

```
sudo chmod +x /usr/local/bin/docker-compose
```

```
mkdir owncloud_files
cd owncloud_files/
```

Pour les fichiers et le contenu du docker-compose.yml et de la configuration d'apache nous nous sommes basé, sur [l'installation d'owncloud](https://gitlab.com/Fireginger/projet-infra-b2-ynov/-/blob/main/Axe%20d'am%C3%A9lioration/owncloud.md).

- nano my-apache.conf
```
Listen 0.0.0.0:80

<VirtualHost *:80>
    ServerName localhost

    DocumentRoot /var/www/html/owncloud

    <Directory /var/www/html/owncloud>
        Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        Require all granted
    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

- nano docker-compose.yml

```
version: '3'
services:
  db:
    image: mariadb:latest
    environment:
      MYSQL_ROOT_PASSWORD: P@ssword
      MYSQL_DATABASE: owncloud_db
      MYSQL_USER: owncloud_user
      MYSQL_PASSWORD: P@ssword
    volumes:
      - ./db:/var/lib/mysql
      - ./my-apache.conf:/etc/mysql/conf.d/my-mariadb.cnf
  app:
    image: httpd:2.4.48
    volumes:
      - ./owncloud:/var/www/html/owncloud
      - ./config/my-apache.conf:/usr/local/apache2/conf/my-apache.conf
    ports:
      - "80:80"
    depends_on:
      - db
```


```
mkdir owncloud
mkdir db
mkdi app
cd owncloud
wget https://download.owncloud.com/server/owncloud-complete-20220919.tar.bz2
tar -jxf owncloud-complete-20220919.tar.bz2
sudo usermod -aG docker baptiste
cd owncloud_files/
docker-compose up -d
```
