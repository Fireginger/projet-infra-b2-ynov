sudo dnf install epel-release -y

sudo dnf install git libuuid-devel autoconf automake pkgconfig zlib-devel curl findutils libmnl gcc make -y

sudo git clone https://github.com/netdata/netdata.git --depth=100

cd netdata

sudo ./packaging/installer/install-required-packages.sh --non-interactive --dont-wait netdata

sudo dnf --enablerepo=powertools install libuv-devel

sudo ./netdata-installer.sh

systemctl start netdata

systemctl enable netdata

sudo firewall-cmd --add-port=19999/tcp --permanent

sudo firewall-cmd --reload